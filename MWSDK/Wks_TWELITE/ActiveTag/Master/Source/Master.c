/*
 * ActiveTag Master.c
 */

#include <AppHardwareApi.h>  // NXP ペリフェラル API 用
#include <stdlib.h>
#include <string.h>
#include <jendefs.h>

#include "utils.h"           // ペリフェラル API のラッパなど
#include "common.h"

#include "serial.h"
#include "sprintf.h"

#include "ToCoNet.h"
#include "ToCoNet_mod_prototype.h"

#include "App_event.h"
//#include "TimeForTransmit.h"


#define APP_ID 0x67726303
#define CHANNEL 18
#define UARD_BAUD 115200
#define UART_PORT_SLAVE E_AHI_UART_0

#define FlushLED  18 // デジタル出力 4

#define MS_SLEEP 1000	// スリープ時間[ms]


#define ID 0x06	// 選手ID (0x01 ~ 0xFE)

static unsigned int sleep_time = 1000;

typedef struct
{
	// MAC
	uint8 u8channel;
	uint16 u16addr;

	// LED Conter;
	uint32 u32LedCt;

	// シーケンス番号
	uint32 u32Seq;

	// スリープカウンタ
	uint8 u8SleepCt;

	bool_t TxFinished;
} tsAppData;

static tsAppData sAppData;
const uint32 u32DioPortWakeUp = 1UL << 7;

tsFILE sSerStream;
tsSerialPortSetup sSerPort;



void vSerialInit(uint32 u32Baud, tsUartOpt *pUartOpt) {
	/* Create the debug port transmit and receive queues */
		static uint8 au8SerialTxBuffer[96];
		static uint8 au8SerialRxBuffer[32];

		/* Initialise the serial port to be used for debug output */
		sSerPort.pu8SerialRxQueueBuffer = au8SerialRxBuffer;
		sSerPort.pu8SerialTxQueueBuffer = au8SerialTxBuffer;
		sSerPort.u32BaudRate = UART_BAUD;
		sSerPort.u16AHI_UART_RTS_LOW = 0xffff;
		sSerPort.u16AHI_UART_RTS_HIGH = 0xffff;
		sSerPort.u16SerialRxQueueSize = sizeof(au8SerialRxBuffer);
		sSerPort.u16SerialTxQueueSize = sizeof(au8SerialTxBuffer);
		sSerPort.u8SerialPort = UART_PORT_SLAVE;
		sSerPort.u8RX_FIFO_LEVEL = E_AHI_UART_FIFO_LEVEL_1;
		SERIAL_vInitEx(&sSerPort, pUartOpt);

		sSerStream.bPutChar = SERIAL_bTxChar;
		sSerStream.u8Device = UART_PORT_SLAVE;
}

static void vInitHardware(int f_warm_start)
{
	// Serial Initialize
	
	vSerialInit(UART_BAUD, NULL);
	ToCoNet_vDebugInit(&sSerStream);
	ToCoNet_vDebugLevel(0);
				/* LEDに接続されている端子を出力端子にする */
    		vPortAsOutput(18);
    		/* 端子をHigh(消灯)にする */
    		vPortSetLo(18);
}

static bool_t sendTxData()
{
	tsTxDataApp tsTx;
	memset(&tsTx, 0, sizeof(tsTxDataApp));

	sAppData.u32Seq++;

	tsTx.u32SrcAddr = ToCoNet_u32GetSerial();	// 自身のアドレス
	tsTx.u32DstAddr = TOCONET_MAC_ADDR_BROADCAST;	// ブロードキャスト

	tsTx.bAckReq = FALSE;
	tsTx.u8Retry = 0x00;	// 3回再送
	tsTx.u8CbId = sAppData.u32Seq & 0xFF;
	tsTx.u8Seq = sAppData.u32Seq & 0xFF;
	tsTx.u8Cmd = TOCONET_PACKET_CMD_APP_DATA;

	SPRINTF_vRewind();
	vfPrintf(SPRINTF_Stream, "%c%c%c", 0xF8, ID, 0xFF);//, sleep_time);
	memcpy(tsTx.auData, SPRINTF_pu8GetBuff(), SPRINTF_u16Length());
	tsTx.u8Len = SPRINTF_u16Length();

	return ToCoNet_bMacTxReq(&tsTx);
}
void transmitData(void)
{
	sendTxData();
	//ToCoNet_Event_SetState(pEv, E_STATE_WAIT_TX);
}

// ユーザ定義のイベントハンドラ
static void vProcessEvCore(tsEvent *pEv, teEvent eEvent, uint32 u32evarg)
{
	switch (pEv->eState) {
	// アイドル状態
	case E_STATE_IDLE:
		if (eEvent == E_EVENT_START_UP) { // 起動時
			// Sleep からの復帰なら稼働状態へ
			// 電源投入直後やリセット後ならそのまま Sleep へ

			if (u32evarg & EVARG_START_UP_WAKEUP_RAMHOLD_MASK) {
				ToCoNet_Event_SetState(pEv, E_STATE_RUNNING);
			} else {
				ToCoNet_Event_SetState(pEv, E_STATE_FINISHED);
			}
		}
		break;

	// 稼働状態
	case E_STATE_RUNNING:
		if (eEvent == E_ORDER_KICK) {
			if (u32evarg == TRUE) {
				// state を送信完了待ちにして送信を実行
				/* 端子をHigh(消灯)にする */
    			vPortSetLo(18);
				sendTxData();
				ToCoNet_Event_SetState(pEv, E_STATE_WAIT_TX);
			} else {
				// Sleep へ
				ToCoNet_Event_SetState(pEv, E_STATE_FINISHED);
			}
		}
		break;

	// 送信完了待ち状態
	case E_STATE_WAIT_TX:
		if (eEvent == E_EVENT_NEW_STATE) {
			// 送信完了につき Sleep 状態へ移行
			vPortSetHi(18);
			SERIAL_vFlush(sSerStream.u8Device);
		}
		if (sAppData.TxFinished) {
			sAppData.TxFinished = FALSE;
			ToCoNet_Event_SetState(pEv, E_STATE_FINISHED);
		}
		break;

	case E_STATE_FINISHED:
		if (eEvent == E_EVENT_NEW_STATE) {
			ToCoNet_Event_SetState(pEv, E_STATE_APP_SLEEPING);
		}
		break;

	// sleep への移行状態
	case E_STATE_APP_SLEEPING:
		if (eEvent == E_EVENT_NEW_STATE) {

			vAHI_DioSetDirection(PORT_INPUT_MASK, 0);
			(void)u32AHI_DioInterruptStatus();
			// vAHI_DioWakeEdge(0, PORT_INPUT_MASK); // 割り込みエッジ（立下りに設定）
			vAHI_DioWakeEdge(PORT_INPUT_MASK, 0); // 割り込みエッジ（立上がりに設定）
			// vAHI_DioWakeEnable(0, PORT_INPUT_MASK); // DISABLE DIO WAK
			sleep_time = (rand() % 401) + 800;
			//sleep_time = (rand() % 2501) + 500;
			// スリープ実行
			ToCoNet_vSleep(E_AHI_WAKE_TIMER_0, sleep_time, TRUE, FALSE);
		}
		break;

	default:
		break;
	}
	return;
}

//
// 以下 ToCoNet 既定のイベントハンドラ群
//

// 割り込み発生後に随時呼び出される
void cbToCoNet_vMain(void)
{
	ToCoNet_Event_Process(E_ORDER_KICK, TRUE, vProcessEvCore);
	return;
}

// パケット受信時
void cbToCoNet_vRxEvent(tsRxDataApp *pRx)
{
    return;
}

// パケット送信完了時
void cbToCoNet_vTxEvent(uint8 u8CbId, uint8 bStatus)
{
	sAppData.TxFinished = TRUE;
	ToCoNet_Event_Process(E_ORDER_KICK, FALSE, vProcessEvCore);
	return;
}

// ネットワークイベント発生時
void cbToCoNet_vNwkEvent(teEvent eEvent, uint32 u32arg)
{
    return;
}

// ハードウェア割り込み発生後（遅延呼び出し）
void cbToCoNet_vHwEvent(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
    return;
}

// ハードウェア割り込み発生時
uint8 cbToCoNet_u8HwInt(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
    return FALSE;
}

// コールドスタート時
void cbAppColdStart(bool_t bAfterAhiInit)
{
	if (!bAfterAhiInit) {
		ToCoNet_REG_MOD_ALL();
	} else {
		vAHI_BrownOutConfigure(0, FALSE, FALSE, FALSE, FALSE);

		memset(&sAppData, 0x00, sizeof(sAppData));

		sAppData.u8channel = CHANNEL;
		sToCoNet_AppContext.u32AppId = APP_ID;
		sToCoNet_AppContext.u8Channel = CHANNEL;
		sToCoNet_AppContext.bRxOnIdle = FALSE;
		sToCoNet_AppContext.u8TxPower = 2;	/*
		 	 	 	 	 	 	 	 	 	 * 0:-34.5db
		 	 	 	 	 	 	 	 	 	 * 1:-23db
		 	 	 	 	 	 	 	 	 	 * 2:-11.5db
		 	 	 	 	 	 	 	 	 	 * 3:最大
		 	 	 	 	 	 	 	 	 	 *  規定値は3(最大)
		 	 	 	 	 	 	 	 	 	 */

		SPRINTF_vInit128();

        // ユーザ定義のイベントハンドラを登録
        ToCoNet_Event_Register_State_Machine(vProcessEvCore);
        sAppData.TxFinished = FALSE;

		/* 送信感覚を揺らがせるタイマを初期化 */
//		TimerForTX_initialize();
		/* コールバックする関数を追加する */
//		TimerForTX_addCallback(transmitData);

		/*乱数の設定 */
		srand(ToCoNet_u32GetSerial());
        vInitHardware(FALSE);

        ToCoNet_vMacStart();
    }
}

// ウォームスタート時
void cbAppWarmStart(bool_t bAfterAhiInit)
{
	if (!bAfterAhiInit) {
	} else {
		vAHI_BrownOutConfigure(0, FALSE, FALSE, FALSE, FALSE);

		vInitHardware(TRUE);

		// MAC start
		ToCoNet_vMacStart();
	}
}