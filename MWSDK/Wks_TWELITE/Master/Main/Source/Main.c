/* Copyright (C) 2017 Mono Wireless Inc. All Rights Reserved.    *
 * Released under MW-SLA-*J,*E (MONO WIRELESS SOFTWARE LICENSE   *
 * AGREEMENT).                                                   */

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/
#include <string.h>

#include <jendefs.h>
#include <AppHardwareApi.h>

#include "utils.h"

#include "Main.h"
#include "config.h"
#include "Version.h"

// DEBUG options

#include "serial.h"
#include "fprintf.h"
#include "sprintf.h"


#include "SMBus.h"


/****************************************************************************/
/***        ToCoNet Definitions                                           ***/
/****************************************************************************/
// Select Modules (define befor include "ToCoNet.h")
//#define ToCoNet_USE_MOD_NBSCAN // Neighbour scan module
//#define ToCoNet_USE_MOD_NBSCAN_SLAVE

// includes
#include "ToCoNet.h"
#include "ToCoNet_mod_prototype.h"

#include "app_event.h"

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

typedef struct
{
    // MAC
    uint8 u8channel;
    uint16 u16addr;

    // LED Counter
    uint32 u32LedCt;

    // シーケンス番号
    uint32 u32Seq;

    // スリープカウンタ
    uint8 u8SleepCt;
} tsAppData;


/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

static void vProcessEvCore(tsEvent *pEv, teEvent eEvent, uint32 u32evarg);

static void vInitHardware(int f_warm_start);

void vSerialInit(uint32 u32Baud, tsUartOpt *pUartOpt);
static void vHandleSerialInput(void);


/****************************************************************************/
/***   自分の関数，へんすう                                                  */
/****************************************************************************/
//ピックにデータがあるか調べる
void get_picData();
#define COUNT_TIME 500
#define SLAVE_NUM 6 //マスターデバイスと接続するスレーブの数
uint8 r_data[SLAVE_NUM]; //picからの2byteのデータ
int count=0;//一秒ごとにpicからデータを要求するためのカウンタ


int16 i16TransmitPingMessage(uint8 *pMsg);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
/* Version/build information. This is not used in the application unless we
   are in serial debug mode. However the 'used' attribute ensures it is
   present in all binary files, allowing easy identifaction... */

/* Local data used by the tag during operation */
static tsAppData sAppData;

PUBLIC tsFILE sSerStream;
tsSerialPortSetup sSerPort;

// Wakeup port
const uint32 u32DioPortWakeUp = 1UL << 7; // UART Rx Port

// センサー状況
#define KICKED_SENSOR_SHT21_TEMP 1
#define KICKED_SENSOR_SHT21_HUMD 2
#define KICKED_SENSOR_BH1715 3
#define KICKED_SENSOR_MPL115 4
#define KICKED_SENSOR_ADT7410 5
#define KICKED_SENSOR_LIS3DH 6
#define KICKED_SENSOR_ADXL345 7

#define DO4  9 // デジタル出力 4

static uint8 u8KickedSensor; //!< 開始されたセンサーの種類
static uint32 u32KickedTimeStamp; //! 開始されたタイムスタンプ

/****************************************************************************
 *
 * NAME: AppColdStart
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/
void cbAppColdStart(bool_t bAfterAhiInit)
{
	vPortAsOutput(DO4);
	//static uint8 u8WkState;
	if (!bAfterAhiInit) {
		// before AHI init, very first of code.

		// Register modules
		ToCoNet_REG_MOD_ALL();

	} else {
		// disable brown out detect
		vAHI_BrownOutConfigure(0,//0:2.0V 1:2.3V
				FALSE,
				FALSE,
				FALSE,
				FALSE);

		// clear application context
		memset (&sAppData, 0x00, sizeof(sAppData));
		sAppData.u8channel = CHANNEL;

		// ToCoNet configuration
		sToCoNet_AppContext.u32AppId = APP_ID;
		sToCoNet_AppContext.u8Channel = CHANNEL;

		sToCoNet_AppContext.bRxOnIdle = TRUE;

		// others
		SPRINTF_vInit128();

		// Register
		ToCoNet_Event_Register_State_Machine(vProcessEvCore);

		// Others
		vInitHardware(FALSE);

		// MAC start
		ToCoNet_vMacStart();

		
	}
}

/****************************************************************************
 *
 * NAME: AppWarmStart
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/
static bool_t bWakeupByButton;


void cbAppWarmStart(bool_t bAfterAhiInit)
{
	if (!bAfterAhiInit) {
		// before AHI init, very first of code.
		//  to check interrupt source, etc.
		bWakeupByButton = FALSE;

		if(u8AHI_WakeTimerFiredStatus()) {
			// wake up timer
		} else
		if(u32AHI_DioWakeStatus() & u32DioPortWakeUp) {
			// woke up from DIO events
			bWakeupByButton = TRUE;
		} else {
			bWakeupByButton = FALSE;
		}
	} else {
		// Initialize hardware
		vInitHardware(TRUE);

		// MAC start
		ToCoNet_vMacStart();
	}
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/
/****************************************************************************
 *
 * NAME: vMain
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/
void cbToCoNet_vMain(void)
{
	/* handle uart input */
	vHandleSerialInput();
}

/****************************************************************************
 *
 * NAME: cbToCoNet_vNwkEvent
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
void cbToCoNet_vNwkEvent(teEvent eEvent, uint32 u32arg) {
	switch(eEvent) {
	default:
		break;
	}
}

/****************************************************************************
 *
 * NAME: cbvMcRxHandler
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/
void cbToCoNet_vRxEvent(tsRxDataApp *pRx) {
	
}

/****************************************************************************
 *
 * NAME: cbvMcEvTxHandler
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
void cbToCoNet_vTxEvent(uint8 u8CbId, uint8 bStatus) {
	return;
}

/****************************************************************************
 *
 * NAME: cbToCoNet_vHwEvent
 *
 * DESCRIPTION:
 * Process any hardware events.
 *
 * PARAMETERS:      Name            RW  Usage
 *                  u32DeviceId
 *                  u32ItemBitmap
 *
 * RETURNS:
 * None
 *
 * NOTES:
 * None.
 ****************************************************************************/

void cbToCoNet_vHwEvent(uint32 u32DeviceId, uint32 u32ItemBitmap)
{
	switch (u32DeviceId) {
	case E_AHI_DEVICE_TICK_TIMER:
		// LED BLINK
		//これ一秒ごとに切り替わってる
		//vPortSet_TrueAsLo(PORT_KIT_LED2, u32TickCount_ms & 0x400);
		if (u32TickCount_ms - sAppData.u32LedCt < 300) {
			vPortSetLo(PORT_KIT_LED1);
		} else {
			vPortSetHi(PORT_KIT_LED1);
		}
		count++;
		//COUNT_TIME[ms]ごとにpicからデータを受信する i2cで
		if(count>COUNT_TIME)
		{
			get_picData();
			count=0;
		}

		if (u8KickedSensor && (u32TickCount_ms - u32KickedTimeStamp) < 0x80000000) { // タイムアウトした
			int16 i16res;

			SPRINTF_vRewind();

			u8KickedSensor = 0;

			vfPrintf(&sSerStream, "%s", SPRINTF_pu8GetBuff());
			i16TransmitPingMessage(SPRINTF_pu8GetBuff());
		}
		break;

	default:
		break;
	}
}

/****************************************************************************
 *
 * NAME: cbToCoNet_u8HwInt
 *
 * DESCRIPTION:
 *   called during an interrupt
 *
 * PARAMETERS:      Name            RW  Usage
 *                  u32DeviceId+
 *                  u32ItemBitmap
 *
 * RETURNS:
 *                  FALSE -  interrupt is not handled, escalated to further
 *                           event call (cbToCoNet_vHwEvent).
 *                  TRUE  -  interrupt is handled, no further call.
 *
 * NOTES:
 *   Do not put a big job here.
 ****************************************************************************/
uint8 cbToCoNet_u8HwInt(uint32 u32DeviceId, uint32 u32ItemBitmap) {
	return FALSE;
}

/****************************************************************************
 *
 * NAME: vInitHardware
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/
static void vInitHardware(int f_warm_start)
{
	// Serial Initialize
#if 0
	// UART の細かい設定テスト
	tsUartOpt sUartOpt;
	memset(&sUartOpt, 0, sizeof(tsUartOpt));
	sUartOpt.bHwFlowEnabled = FALSE;
	sUartOpt.bParityEnabled = E_AHI_UART_PARITY_ENABLE;
	sUartOpt.u8ParityType = E_AHI_UART_EVEN_PARITY;
	sUartOpt.u8StopBit = E_AHI_UART_2_STOP_BITS;
	sUartOpt.u8WordLen = 7;

	vSerialInit(UART_BAUD, &sUartOpt);
#else
	vSerialInit(UART_BAUD, NULL);
#endif


	ToCoNet_vDebugInit(&sSerStream);
	ToCoNet_vDebugLevel(0);

	// SMBUS
	vSMBusInit();
}

/****************************************************************************
 *
 * NAME: vInitHardware
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/
void vSerialInit(uint32 u32Baud, tsUartOpt *pUartOpt) {
	/* Create the debug port transmit and receive queues */
	static uint8 au8SerialTxBuffer[864];
	static uint8 au8SerialRxBuffer[128];

	/* Initialise the serial port to be used for debug output */
	sSerPort.pu8SerialRxQueueBuffer = au8SerialRxBuffer;
	sSerPort.pu8SerialTxQueueBuffer = au8SerialTxBuffer;
	sSerPort.u32BaudRate = u32Baud;
	sSerPort.u16AHI_UART_RTS_LOW = 0xffff;
	sSerPort.u16AHI_UART_RTS_HIGH = 0xffff;
	sSerPort.u16SerialRxQueueSize = sizeof(au8SerialRxBuffer);
	sSerPort.u16SerialTxQueueSize = sizeof(au8SerialTxBuffer);
	sSerPort.u8SerialPort = UART_PORT_SLAVE;
	sSerPort.u8RX_FIFO_LEVEL = E_AHI_UART_FIFO_LEVEL_1;
	SERIAL_vInitEx(&sSerPort, pUartOpt);

	sSerStream.bPutChar = SERIAL_bTxChar;
	sSerStream.u8Device = UART_PORT_SLAVE;
}

/****************************************************************************
 *
 * NAME: vHandleSerialInput
 *
 * DESCRIPTION:
 *
 * PARAMETERS:      Name            RW  Usage
 *
 * RETURNS:
 *
 * NOTES:
 ****************************************************************************/
static void vHandleSerialInput(void)
{
	// handle UART command
	while (!SERIAL_bRxQueueEmpty(sSerPort.u8SerialPort)) {
		int16 i16Char;

		i16Char = SERIAL_i16RxChar(sSerPort.u8SerialPort);

		vfPrintf(&sSerStream, "\n\r# [%c] --> ", i16Char);
	    SERIAL_vFlush(sSerStream.u8Device);

		switch(i16Char) {


		case '>': case '.':
			/* channel up */
			sAppData.u8channel++;
			if (sAppData.u8channel > 26) sAppData.u8channel = 11;
			sToCoNet_AppContext.u8Channel = sAppData.u8channel;
			ToCoNet_vRfConfig();
			vfPrintf(&sSerStream, "set channel to %d.", sAppData.u8channel);
			break;

		case '<': case ',':
			/* channel down */
			sAppData.u8channel--;
			if (sAppData.u8channel < 11) sAppData.u8channel = 26;
			sToCoNet_AppContext.u8Channel = sAppData.u8channel;
			ToCoNet_vRfConfig();
			vfPrintf(&sSerStream, "set channel to %d.", sAppData.u8channel);
			break;

		case 'd': case 'D':
			_C {
				get_picData();
			}
			break;

		case 't': // パケット送信してみる
			_C {
			}
			break;

		default:
			break;
		}

		vfPrintf(&sSerStream, LB);
	    SERIAL_vFlush(sSerStream.u8Device);
	}
}

int16 i16TransmitPingMessage(uint8 *pMsg) {
	// transmit Ack back
	tsTxDataApp tsTx;
	memset(&tsTx, 0, sizeof(tsTxDataApp));
	uint8 *q = tsTx.auData;

	sAppData.u32Seq++;

	tsTx.u32SrcAddr = ToCoNet_u32GetSerial(); // 自身のアドレス
	tsTx.u32DstAddr = 0xFFFF; // ブロードキャスト

	tsTx.bAckReq = FALSE;
	tsTx.u8Retry = 0x82; // ブロードキャストで都合３回送る
	tsTx.u8CbId = sAppData.u32Seq & 0xFF;
	tsTx.u8Seq = sAppData.u32Seq & 0xFF;
	tsTx.u8Cmd = TOCONET_PACKET_CMD_APP_DATA;

	// SPRINTF でメッセージを作成
	S_OCTET('P');
	S_OCTET('I');
	S_OCTET('N');
	S_OCTET('G');
	S_OCTET(':');
	S_OCTET(' ');

	uint8 u8len = strlen((const char *)pMsg);
	memcpy(q, pMsg, u8len);
	q += u8len;
	tsTx.u8Len = q - tsTx.auData;

	// 送信
	if (ToCoNet_bMacTxReq(&tsTx)) {
		// LEDの制御
		sAppData.u32LedCt = u32TickCount_ms;

		// ＵＡＲＴに出力
		vfPrintf(&sSerStream, LB "Fire PING Broadcast Message.");

		return tsTx.u8CbId;
	} else {
		return -1;
	}
}

/****************************************************************************
 *
 * NAME: vProcessEvent
 *
 * DESCRIPTION:
 *
 * RETURNS:
 *
 ****************************************************************************/

// DO4 の Lo / Hi をトグル
  
static void vProcessEvCore(tsEvent *pEv, teEvent eEvent, uint32 u32evarg) {
	// 1 秒周期のシステムタイマ通知
	if (eEvent == E_EVENT_TICK_SECOND) {
		//vfPrintf(&sSerStream, "\r\n*** 6 minutes walking master ***");
    }
	if (eEvent == E_EVENT_START_UP) {
		// ここで UART のメッセージを出力すれば安全である。
		if (u32evarg & EVARG_START_UP_WAKEUP_RAMHOLD_MASK) {
			vfPrintf(&sSerStream, LB "RAMHOLD");
		}
	    if (u32evarg & EVARG_START_UP_WAKEUP_MASK) {
			vfPrintf(&sSerStream, LB "Wake up by %s. SleepCt=%d",
					bWakeupByButton ? "UART PORT" : "WAKE TIMER",
					sAppData.u8SleepCt);
	    } else {
			//リセットボタン押したときに出る文字
	    	vfPrintf(&sSerStream, "\r\n*** 6 minutes walking master 1024 ***");
	    	//vfPrintf(&sSerStream, "\r\n*** %08x ***", ToCoNet_u32GetSerial());
	    }
	}
	
    
}
 //スレーブに対してi2c readを送り，ibyte取得 受信したIDの仕分け これ機能分割した方がいいかも
void get_picData()
{
	int i;
	int received_dara_num = 0; //受信したIDの数を格納
	uint8 slave_addr = 0x2a;
	
	for(i=0;i<SLAVE_NUM;i++)
		r_data[i]=0;//スレーブからの値を初期化
	//0x7C（0111 110 0）と書かれていますが、bSMBusWriteには、0x3E（011 1110）を指定する必要がある
	for (i = 0; i < 3; i++)
	{
		bSMBusSequentialRead(slave_addr, 2, &r_data[2*i]); //スレーブのアドレス，サイズ，入れる変数
		slave_addr += 0x10;								 //0x2a 0x3a 0x4aとアドレスを変える
		if (r_data[2*i] != 0x00)
			received_dara_num++; //受信したIDの数をインクリメント
		if (r_data[2*i+1] != 0x00)
			received_dara_num++; //受信したIDの数をインクリメント
	}
	// bSMBusSequentialRead(slave_addr, 2, &r_data[0]); //スレーブのアドレス，サイズ，入れる変数
	//3つのスレーブのうちどれかが受信していたら3つのデバイスの値を表示
	if(received_dara_num!=0)
		vfPrintf(&sSerStream, "\n\r%d:%02x%02x  %02x%02x  %02x%02x",received_dara_num,r_data[0],r_data[1],r_data[2],r_data[3],r_data[4],r_data[5]);
	
	

	/**********************************************************
	//
	//ここからIDの仕分け 
	// 
	*************************************************************/
/*
	switch (received_dara_num)
	{
	case 1: //1つのIDを受信
		for (i = 0; i < SLAVE_NUM; i++)
		{
			if (r_data[i] != 0x00)
			{
				vfPrintf(&sSerStream, "%02x", r_data[i]);
				break;
			}
		}
		break;

	case 2: //2つのIDを受信
		//受信した2つのIDが同じ場合
		if (r_data[0] == r_data[1]) //1番目,2番目が同じIDなら
		{
			vfPrintf(&sSerStream, "%02x", r_data[0]);
		}
		else if (r_data[0] == r_data[2]) //1番目,3番目が同じIDなら
		{
			vfPrintf(&sSerStream, "%02x", r_data[0]);
		}
		else if (r_data[1] == r_data[2]) //2番目,3番目が同じIDなら
		{
			vfPrintf(&sSerStream, "%02x", r_data[1]);
		}
		else if (r_data[0] == 0x00) //1番目が0ならあとは2,3番目が異なるIDを持つ
		{
			vfPrintf(&sSerStream, "%02x %02x", r_data[1], r_data[2]);
		}
		else if (r_data[1] == 0x00) //2番目が0ならあとは1,3番目が異なるIDを持つ
		{
			vfPrintf(&sSerStream, "%02x %02x", r_data[0], r_data[2]);
		}
		else if (r_data[2] == 0x00) //3番目が0ならあとは1,2番目が異なるIDを持つ
		{
			vfPrintf(&sSerStream, "%02x %02x", r_data[0], r_data[1]);
		}
		break;

	case 3:
		if (r_data[0] == r_data[1] && r_data[1]==r_data[2])//3つとも全部同じ
		{
			vfPrintf(&sSerStream, "%02x", r_data[0]);
		}
		else if (r_data[0] == r_data[1]) //1番目,2番目が同じIDなら
		{
			vfPrintf(&sSerStream, "%02x %02x", r_data[0],r_data[2]);
		}
		else if (r_data[0] == r_data[2]) //1番目,3番目が同じIDなら
		{
			vfPrintf(&sSerStream, "%02x %02x", r_data[0],r_data[1]);
		}
		else if (r_data[1] == r_data[2]) //2番目,3番目が同じIDなら
		{
			vfPrintf(&sSerStream, "%02x %02x", r_data[0],r_data[1]);
		}
		else//3つともすべて違うID 
		{
			vfPrintf(&sSerStream, "%02x %02x %02x", r_data[0],r_data[1],r_data[2]);
		}
		break;
	default:
		break;
	}*/
}
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
