/*
 * File:   Receive.c
 * Author: hirose
 *
 * Created on 2019/05/17, 14:48
 */

#include "pic.h"
#include <xc.h>
#include "Interrupt.h"
#include "IIC.h"
#include "Receive.h"

/*
int main(void) {
    /* Replace with your application code 
    while (1) {
    }
}*/


extern unsigned char data=0;

void Initialize_Receive_Port()
{
    //アナログポートをデジタル(0)に設定
    ANSA0=0;//port 2
    ANSA1=0;//port 3
    ANSA2=0;//port 4
    ANSA3=0;//port 5
    ANSA4=0;//port 6
    ANSB0=0;//port 21
    //入出力を設定1:input 0:output
    TRISA0=1;
    TRISA1=1;
    TRISA2=1;
    TRISA3=1;
    TRISA4=1;
    TRISB0=1;
    
    /* INTに関する割込みが発生したときのリスナを設定する */
    Interrupt_setReceiveListener(twelite_data_interruptListener);
}

void twelite_data_interruptListener()
{
    data=PORTA;
}