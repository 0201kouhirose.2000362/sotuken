#ifndef _INTERVALTIMER_H_
#define _INTERVALTIMER_H_

void IntervalTimer_initialize();
void IntervalTimer_setTime(unsigned short ms);
void IntervalTimer_setCallback( void (*callback)() );
void IntervalTimer_start();
void IntervalTimer_stop();
extern void IntervalTimer_interruptListener();

#endif /* _INTERVALTIMER_H_ */