#include "pic.h"
#include "DotMatrixDisplay.h"
#include "IntervalTimer.h"
#include "Boolean.h"
#include "IIC.h"

/**
 * 画面をリフレッシュする時間の間隔(ns)
 */
#define _INTERVAL_TIME (500)

/**
 * 定型のパターンであるか
 */
#define _IS_FORMAT_PATTERN (0x80)

/**
 * オリジナルパターンであるか
 */
#define _IS_ORIGINAL_PATTERN (0x01)

/**
 * 表示するパターン
 */
static ShapeType _shapeType = NO_CROSS;

/**
 * 交差点の情報を構造体に入れる
 */
static const Pattern _SHAPE_PATTERN[]={
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // 全消灯
    {0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xFF, 0xFF, 0xFF}, // ┓形
    {0xFF, 0xFF, 0xFF, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0}, // ┏形
    {0x07, 0x07, 0x07, 0x07, 0x07, 0xFF, 0xFF, 0xFF}, // ┛形
    {0xFF, 0xFF, 0xFF, 0x07, 0x07, 0x07, 0x07, 0x07}, // ┗形
    {0xFF, 0xFF, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18}, // ┣形
    {0xC0, 0xC0, 0xC0, 0xFF, 0xFF, 0xC0, 0xC0, 0xC0}, // ┳形
    {0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0xFF, 0xFF}, // ┫形
    {0x03, 0x03, 0x03, 0xFF, 0xFF, 0x03, 0x03, 0x03}, // ┻形
    {0x18, 0x18, 0x18, 0xFF, 0xFF, 0x18, 0x18, 0x18}, // ╋形
    {0xFF, 0xFF, 0xC3, 0xC3, 0xC3, 0xC3, 0xFF, 0xFF}, // 四角形
    {0x18, 0x3C, 0x66, 0xC3, 0xC3, 0x66, 0x3C, 0x18}, // ひし形
    {0x42, 0xE7, 0x7E, 0x3C, 0x3C, 0x7E, 0xE7, 0x42}, // ×
    {0xE0, 0xFC, 0x1E, 0x07, 0x07, 0x1E, 0xFC, 0xE0}, // V字
    {0x00, 0x00, 0x18, 0x3C, 0x3C, 0x18, 0x00, 0x00}, // ・
    {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}// 全点灯
};

/**
 * オリジナルパターン
 */
static Pattern _originalPattern;

/**
 * オリジナルパターンを表示しているか
 */
static boolean _enableOriginalPattern = false;

/**
 * シフトレジスタを操作する
 * @param column 列番号
 */
static void _controlShiftRegister(unsigned char column);

/**
 * 列データを設定する
 * @param column_data 列データ
 */
static void _setColumnData(unsigned char column_data);

/**
 * 列データを消す
 */
static void _clearColumn(void);

/**
 * 表示の更新
 */
static void _refreshDisplay();

/**
 * 画面をクリアする
 */
static void _clear(void);

/**
 * 受信終了かチェックする関数
 * @param data 受信データ
 * @param length 受信データの長さ
 * @return 受信完了なら真，未完了なら偽を返す
 */
static boolean _checker(volatile unsigned char *data, volatile unsigned char length);

/**
 * パターンを受信したときに呼び出す関数
 * @param data 受信データ
 * @param length 受信データの長さ
 */
static void _receivePattern(volatile unsigned char *data, volatile unsigned char length);

void DotMatrixDisplay_initialize(void) {

    /* インターバルタイマを初期化する */
    IntervalTimer_initialize();

    /* ポートAビット4を出力端子とする */
    TRISA4 = 0;
    /* ポートAビット5を出力端子とする */
    TRISA5 = 0;

    /* ポートAビット6を出力端子とする */
    TRISA6 = 0;
    /* ポートCビット0を出力端子とする */
    TRISC0 = 0;
    /* ポートCビット1を出力端子とする */
    TRISC1 = 0;
    /* ポートCビット2を出力端子とする */
    TRISC2 = 0;
    /* ポートCビット5を出力端子とする */
    TRISC5 = 0;
    /* ポートAビット7を出力端子とする */
    TRISA7 = 0;
    /* ポートBビット1をデジタルピンとして使用する */
    ANSB1 = 0;
    /* ポートBビット1を出力端子とする */
    TRISB1 = 0;
    /* ポートBビット2をデジタルピンとして使用する */
    ANSB2 = 0;
    /* ポートBビット2を出力端子とする */
    TRISB2 = 0;

    /* インターバルタイマに割り込ませる間隔を設定する */
    IntervalTimer_setTime(_INTERVAL_TIME);
    /* インターバルタイマにコールバック関数を設定する */
    IntervalTimer_setCallback(_refreshDisplay);

    /* データを受信したときに呼び出す関数を設定する */
    IIC_setReceiveDataListener(_receivePattern, _checker);
}

void DotMatrixDisplay_setPattern(ShapeType type) {
    _shapeType = type;
    _enableOriginalPattern = false;
}

void DotMatrixDisplay_setOriginalPattern(Pattern *original_pattern) {
    unsigned char i;
    for(i=0; i<8; i++) {
        _originalPattern._column[i] = original_pattern->_column[i];
    }
    _enableOriginalPattern = true;
}

static void _clear(void){
    LATA4 = 0;
    LATA5 = 0;
    for(int i=0; i<8;i++){
        LATA5 = 1;
        LATA5 = 0;
    }
}

void DotMatrixDisplay_turnOn(void) {
    IntervalTimer_start();
}

void DotMatrixDisplay_turnOff(void) {
    IntervalTimer_stop();
    _clear();
}

static void _controlShiftRegister(unsigned char column) {
    /* 列番号が0の場合 */
    if (column == 0) {
        /* シフトレジスタに入れられる信号を1とする */
        LATA4 = 1;
    } else {
        /* シフトレジスタに入れられる信号を0とする */
        LATA4 = 0;
    }

    /* シフトさせる */
    LATA5 = 1;
    LATA5 = 0;
}

static void _setColumnData(unsigned char column_data){
     /* ディスプレイの上方 */
     LATA6 = ((column_data&0x80) ? 1:0);
     LATC0 = ((column_data&0x40) ? 1:0);
     LATC1 = ((column_data&0x20) ? 1:0);
     LATC2 = ((column_data&0x10) ? 1:0);
     LATC5 = ((column_data&0x08) ? 1:0);
     LATA7 = ((column_data&0x04) ? 1:0);
     LATB1 = ((column_data&0x02) ? 1:0);
     LATB2 = ((column_data&0x01) ? 1:0);
     /* ディスプレイの下方 */
}

static void _clearColumn(void) {
    LATA6 = 0;
    LATC0 = 0;
    LATC1 = 0;
    LATC2 = 0;
    LATC5 = 0;
    LATA7 = 0;
    LATB1 = 0;
    LATB2 = 0;
}

static void _refreshDisplay(void){
    unsigned char column = 0;

    /* 表示するパターン */
    const Pattern *display_pattern;

    /* オリジナルパターンを表示する場合 */
    if(_enableOriginalPattern){
        display_pattern = &_originalPattern;
    }
    /* 定型のパターンを表示する場合 */
    else{
        display_pattern = &(_SHAPE_PATTERN[_shapeType]);
    }

    for(column=0; column<8; column++) {
        /* 列の表示／非表示を外部のシフトレジスタで行っているので */
        /* そのシフトレジスタを操作する */
        _controlShiftRegister(column);

        _setColumnData(display_pattern->_column[column]);

        /* 点灯している時間だけウェイトを入れる */
        /* つまり，このウェイトが長ければ明るくなる */
        _nop();
        _nop();

        /* 表示を消す */
        /* このようにしないと，前の列の残像が残ってしまう */
        _clearColumn();
    }
}

static boolean _checker(volatile unsigned char *data, volatile unsigned char length){
    if(length >= 2){
        /* 定型のパターンであるか */
        if(data[0] == _IS_FORMAT_PATTERN){
            return true;
        }
        /* オリジナルパターンであるか */
        else if(data[0] == _IS_ORIGINAL_PATTERN){
            /* データの長さが9となったら */
            if(length == 9){
                return true;
            }
        }
    }
    return false;

}

static void _receivePattern(volatile unsigned char *data, volatile unsigned char length){
    if(length >=2){
        /* 定型のパターンであるか */
        if(data[0] == _IS_FORMAT_PATTERN){
            /* 一旦，表示を消す */
            DotMatrixDisplay_turnOff();
            /* 表示するパターンを設定する */
            DotMatrixDisplay_setPattern(data[1]);
            /* 表示する */
            DotMatrixDisplay_turnOn();
        }
        /* オリジナルパターンであるか */
        else{
            /* データの長さが9となったら */
            if(length == 9) {
                Pattern original_pattern;
                unsigned char i;

                /* 一旦，表示を消す */
                DotMatrixDisplay_turnOff();

                for(i=0; i<8; i++) {
                    original_pattern._column[i] = data[i+1];
                }
                DotMatrixDisplay_setOriginalPattern(&original_pattern);
                
                /* 表示する */
                DotMatrixDisplay_turnOn();
            }
        }
    }
}