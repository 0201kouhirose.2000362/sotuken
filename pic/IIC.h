/* 
 * File:   IIC.h
 * Author: ashida
 *
 * Created on 2013/08/14, 9:59
 */

#ifndef _IIC_H_
#define	_IIC_H_

#include"Boolean.h"

/**
 * I2Cの初期化を行う
 */
extern void IIC_initialize();

/**
 * I2Cに関する割り込みが発生したときに行う処理
 */
extern void IIC_eventHandler();

/**
 * 受信終了時に呼び出すハンドラを設定する
 * @param callback コールバック関数
 * @param checker 受信終了か調べる関数
 */
extern void IIC_setReceiveDataListener(
        void (*callback)(volatile unsigned char *data, volatile unsigned char length),
        boolean (*checker)(volatile unsigned char *data, volatile unsigned char length));

/**
 * 送信終了時に呼び出すハンドらを設定する
 * @param calback コールバック関数
 */
extern void IIC_setTransmitDataListener( void(*callback)());

/**
 * 送信するデータを設定する
 * @param data 送信するデータ
 * @param length 送信するデータの長さ
 */
extern void IIC_setTransmitData(unsigned char *data, unsigned char length);

#endif	/* _IIC_H_ */

