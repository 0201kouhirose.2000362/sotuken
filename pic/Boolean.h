#ifndef _BOOLEAN_H_
#define _BOOLEAN_H_

typedef unsigned char boolean;

#define true (1)
#define false (0)

#endif