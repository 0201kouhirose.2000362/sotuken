/* 
 * File:   Interrupt.h
 * Author: ashida
 *
 * Created on 2013/08/14, 10:19
 */

#ifndef _INTERRUPT_H_
#define	_INTERRUPT_H_

/**
 * Timer0のイベントリスナを設定する
 * @param listener リスナ
 */
void Interrupt_setTimer0Listener( void(*listener)());


/**
 * I2Cに関する割り込みが発生したときに呼び出す関数を設定する
 * @param listener 呼び出される関数
 */
void Interrupt_setI2CListener( void(*listener)());

void Interrupt_setReceiveListener(void (*listener)());

#endif	/* _INTERRUPT_H_ */

