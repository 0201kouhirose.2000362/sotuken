/*
 * File:   newmainpicc.c
 * Author: ashida
 *
 * Created on 2011/08/24, 17:39
 */

// PIC16F1936 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable (Brown-out Reset disabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = ON        // Internal/External Switchover (Internal/External Switchover mode is enabled)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config VCAPEN = OFF     // Voltage Regulator Capacitor Enable (All VCAP pin functionality is disabled)
#pragma config PLLEN = ON       // PLL Enable (4x PLL enabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include "pic.h"
#include "Interrupt.h"
#include "IIC.h"
#include "DotMatrixDisplay.h"
#include "Receive.h"





int main(void) {

    /* 内部オシレータの速度を4[MHz]とする */
    IRCF0 = 1;
    IRCF1 = 0;
    IRCF2 = 1;
    IRCF3 = 1;

    /* LCD を制御しないようにする */
    LCDEN = 0;
    
    /*picの受信端子を設定*/
    Initialize_Receive_Port();
    
     
    
    /* ドットマトリクスディスプレイを初期化する */
    DotMatrixDisplay_initialize();

    /* I2Cの初期化をする */
    IIC_initialize();

    /* 割り込みを許可する */
    PEIE = 1;
    GIE = 1;
    INTE=1;

    /* パターンを設定する */
    DotMatrixDisplay_setPattern(VFORMATION);
    /* 表示させる */
    DotMatrixDisplay_turnOn();
    {
        /*
        Pattern original_pattern;
        original_pattern._column[0] = 0x00;
        original_pattern._column[1] = 0x04;
        original_pattern._column[2] = 0x02;
        original_pattern._column[3] = 0x00;
        original_pattern._column[4] = 0xff;
        original_pattern._column[5] = 0x00;
        original_pattern._column[6] = 0x00;
        original_pattern._column[7] = 0x00;
        DotMatrixDisplay_setOriginalPattern(&original_pattern);
        */
    }
    while(1) {
    }
    
    return 0;
}

