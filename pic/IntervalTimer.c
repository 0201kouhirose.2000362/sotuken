#include <xc.h>
#include <stdbool.h>
#include "IntervalTimer.h"
#include "Interrupt.h"

#define TIMEUP (1)
#define NOT_TIMEUP (0)
#define TMR0_FRQ (1.0)

static unsigned short _overflowNum;
static unsigned char _tmr0;
static unsigned short _counter;

static void (*_callback)();
static bool _timeUpFlag = NOT_TIMEUP;

void IntervalTimer_initialize(){
    /* 内部命令サイクルクロックを使ってタイマ0のクロックとする */
    /* ただし，タイマ0の仕様により，4分周されるので，1[MHz]となる */
    /* 内部命令サイクルクロックを使用する */
    TMR0CS = 0;
    /* プリスケーラを使わない */
    PSA = 1;
    /* タイマのカウンタをクリアする */
    TMR0 = 0x00;
    /* すべての割り込みを許可する */
    GIE = 1;

    /* タイマ0に関する割込みが発生したときのリスナを設定する */
    Interrupt_setTimer0Listener(IntervalTimer_interruptListener);
}

void IntervalTimer_setTime(unsigned short ns) {
    /* オーバフローする回数を求める */
    _overflowNum = (unsigned short)((double)ns*TMR0_FRQ/256);
    /* オーバフローする回数だけでは表しきれない端数を求める */
    _tmr0 = (unsigned char)(256 - (unsigned char)(((double)ns - (double)_overflowNum /TMR0_FRQ/256)*TMR0_FRQ));
    /* カウンタを設定する．なお，1足しているのは，端数分，カウントするためである */
    _counter = _overflowNum+1;
}
void IntervalTimer_setCallback( void (*callback)() ) {
    _callback = callback;
}

void IntervalTimer_start(){
    /* タイマ0を動作させ始める */
    T0IE = 1;
}

void IntervalTimer_stop(){
    /* タイマ0を停止させる*/
    T0IE = 0;
}

void IntervalTimer_interruptListener() {
    /* カウンタ値を減らしていく */
    --_counter;
    /* カウンタ値が1となったら*/
    if (_counter == 1) {
        /* タイマ0を設定する */
        TMR0 = _tmr0;
    } else if (_counter == 0) {
        /* コールバック関数を呼び出す */
        (*_callback)();
        /* カウンタ値を_overflowNum+1に戻す */
        _counter = _overflowNum + 1;
    }
}
