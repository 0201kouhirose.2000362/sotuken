#include <xc.h>
#include "IIC.h"
#include "Interrupt.h"
#include "Boolean.h"
#include "Receive.h"

/* I2Cの7ビットアドレス */
#define I2C_ADDRESS (0x54)

/**
 * 受信バッファサイズ
 */
#define _RXBUFFER_SIZE (10)

/**
 * 送信バッファサイズ
 */ 
#define _TXBUFFER_SIZE (10)

/**
 * 受信完了時に呼び出されるコールバック関数
 */
static void (*_receiveDataCallback)(volatile unsigned char *, volatile unsigned char);

/**
 * 送信完了時に呼び出されるコールバック関数
 */
static void (*_transmitDataCallback)();

/**
 * 受信バッファ
 */
volatile static unsigned char _rxBuffer[_RXBUFFER_SIZE];
/**
 * 受信バッファのポインタ
 */
volatile static unsigned char _rxBufferIndex = 0;

/**
 * 受信終了か調べる関数
 */
static boolean (*_checker)(volatile unsigned char *, volatile unsigned char);

/**
 * 送信バッファ
 */
volatile static unsigned char _txBuffer[_TXBUFFER_SIZE];

/**
 * 送信バッファのポインタ
 */
volatile static unsigned char _txBufferIndex = 0;

/**
 * 送信するデータの長さ
 */
volatile static unsigned char _txBufferLength = 0;

/**
 * tweliteから受信した1byteのデータ
 */
extern unsigned char data;



/**
 * I2Cによりスレーブ(PIC)からマスタへデータを送る
 * @param data 送信するデータ
 */
static void _i2CWrite(unsigned char data);

/* I2Cの初期化 */
void IIC_initialize() {
    /* SCLおよびSDAを入力端子とする */
    TRISC3 = 1;
    TRISC4 = 1;

    /* クロックストレッチを有効にする */
    SEN = 1;

    SSPCON3 = 0x00;

    /* I2Cアドレスを設定する */
    SSPADD = I2C_ADDRESS;

    /* I2Cに関する状態を初期化する */
    SSPSTAT = 0x00;

    /* 書き込み時のコリジョンをクリアする  */
    WCOL = 0;
    /* オーバフローを示すフラグをクリアする */
    SSPOV = 0;
    /* クロックストレッチをリリースする */
    CKP = 1;
    /* I2Cをスレーブモード，7ビットアドレス， */
    /* スタートビットおよびストップビットのときに割り込ませない  */
    SSPM3 = 0;
    SSPM2 = 1;
    SSPM1 = 1;
    SSPM0 = 0;

    /* 割り込みを許可する */
    SSPIE = 1;

    /* I2Cに関する割り込みが発生したときに呼び出される関数を設定しておく */
    Interrupt_setI2CListener(IIC_eventHandler);

    /* MSSPを使用する  */
    SSPEN =1;
}

void IIC_eventHandler(){
    unsigned char i2cStatus, value;

    /* I2Cの状態を取得する */
    /* このとき，重要でないビットについてはマスクする */
    i2cStatus = (SSPSTAT & 0b00101101);

    // D/A, S, R/W, BF
    switch (i2cStatus) {
            /* 状態1:受信用アドレス */
            /* D/A=0⇒アドレス */
            /* S=1⇒スタートビットを受信 */
            /* R/W=0⇒マスタから見た場合，書込み */
            /* BF=1⇒受信完了 */
        case 0b00001001:
            /* 受信したアドレスを取得する(ただし，特に使わない) */
            value = SSPBUF;
            /* 受信バッファを初期化する */
            for (int i = 0; i < _RXBUFFER_SIZE; i++) {
                _rxBuffer[i] = 0;
            }
            /* 受信バッファのポインタを初期化する */
            _rxBufferIndex = 0;
            /* SCLをリリースする */
            if (SEN) {
                CKP = 1;
            }
            break;
            /* 状態2:データ受信 */
            /* D/A=1⇒データ */
            /* S=1⇒スタートビットを受信した */
            /* R/W=0⇒マスタから見た場合，書込み */
            /* BF=1⇒受信完了 */
        case 0b00101001:
            /* 受信したデータを取得し，受信バッファに入れる */
            _rxBuffer[_rxBufferIndex] = SSPBUF;
            /* 受信バッファのポインタをインクリメントする */
            _rxBufferIndex++;
            /* 受信終了していたら */
            if(_checker(_rxBuffer,_rxBufferIndex)){
                /* 受信終了したことを伝えるイベントハンドらを呼び出す */
                _receiveDataCallback(_rxBuffer, _rxBufferIndex);
            }
            /* SCLをリリースする */
            if (SEN){
                CKP = 1;
            }
            break;
            /* 状態3:送信用アドレス */
            /* D/A=0⇒アドレス */
            /* S=1⇒スタートビットを受信した */
            /* R/W=1⇒マスタから見た場合，読み込み */
            /* BF=0⇒受信していない */
        case 0b00001101: //possibly BF==1
            /* 受信したアドレスを読み込む */
            value = SSPBUF;
            //txbuffer に受信した1byteのデータを格納
            IIC_setTransmitData(&data,1);
            data=0;//i2c割り込み後に，tweliteからのデータを初期化する．
            /* 送信用バッファのインデックスを初期化する */
            _txBufferIndex = 0;
            _i2CWrite(_txBuffer[_txBufferIndex]); //write back the index of status requested
            _txBufferIndex++;
            /* SCLをリリースする */
            if (SEN){
                CKP = 1;
            }
            break;
            /* 状態4:送信用データ */
            /* D/A=1⇒データ */
            /* S=1⇒スタートビットを受信した */
            /* R/W=1⇒マスタから見た場合，読み込み */
            /* BF=0⇒受信していない */
           case 0b00101100:
            //txbuffer に受信した1byteのデータを格納
            IIC_setTransmitData(&data,1);
            /* 送信用バッファに入っているデータを送信する*/   
            _i2CWrite(_txBuffer[_txBufferIndex]);
            /* 送信用バッファのインデックスをインクリメントする */
            _txBufferIndex++;
            /* すべて送信し終えたら */
            if(_txBufferIndex == _txBufferLength) {
                /* 送信し終えたことを伝える関数を呼び出す */
                _transmitDataCallback();
            }
            /* SCLをリリースする */
            if (SEN){
              CKP = 1;
            }
            break;
            //State 5
            //Slave I2C logic reset by NACK from master
            //SSPSTAT bits: D/A=1, S=1, R/W=0, BF=0, CKP=1
        case 0b00101000:
            //txbuffer に受信した1byteのデータを格納
            IIC_setTransmitData(&data,1);
            /* 送信用バッファに入っているデータを送信する*/   
            _i2CWrite(_txBuffer[_txBufferIndex]);
            /* 送信用バッファのインデックスをインクリメントする */
            _txBufferIndex++;
            /* すべて送信し終えたら */
            if(_txBufferIndex == _txBufferLength) {
                /* 送信し終えたことを伝える関数を呼び出す */
                _transmitDataCallback();
            }
            /* SCLをリリースする */
            if (SEN){
              CKP = 1;
            }
            break;
        default:
            break;
    }

}

void IIC_setReceiveDataListener(
        void (*callback)(volatile unsigned char *data, volatile unsigned char length),
        boolean (*checker)(volatile unsigned char *data, volatile unsigned char length)) {
    _receiveDataCallback = callback;
    _checker = checker;
}

void IIC_setTransmitDataListener( void(*callback)()) {
    _transmitDataCallback = callback;
}

void IIC_setTransmitData(unsigned char *data, unsigned char length) {

    unsigned char i;

    /* 送信データをバッファへコピーする */
    for(i=0; i<length; i++){
        _txBuffer[i] = data[i];
    }

    /*送信バッファのインデックスを初期化する */
    _txBufferIndex = 0;
    /* 送信データの長さを記憶する */
    _txBufferLength = length;
}


static void _i2CWrite(unsigned char data){
    while(BF);      //wait while buffer is full
    do{
        WCOL = 0;           //clear write collision flag
        SSPBUF = data;
    }while (WCOL);           //do until write collision flag is clear
    if (SEN) CKP = 1;           //release the SCL line
}
