/* 
 * File:   DotMatrixDisplay.h
 * Author: ashida
 *
 * Created on 2013/08/14, 10:33
 */

#ifndef _DOTMATRIXDISPLAY_H_
#define	_DOTMATRIXDISPLAY_H_

/**
 * ドットマトリクスディスプレイに表示するパターン
 */
typedef enum{
    /**
     * 交差していない
     */
    NO_CROSS = 0,
    /**
     * ┓形の交差点
     */
    UPPER_RIGHT_CORNER = 1,
    /**
     * ┏形の交差点
     */
    UPPER_LEFT_CORNER = 2,
    /**
     * ┛形の交差点
     */
    LOWER_RIGHT_CORNER = 3,
    /**
     * ┗形の交差点
     */
    LOWER_LEFT_CORNER = 4,
    /**
     * ┣形の交差点
     */
    COUNTER_CLOCKWISE_TCROSS = 5,
    /**
     * ┳形の交差点
     */
    TCROSS = 6,
    /**
     * ┫形の交差点
     */
    CLOCKWISE_TCROSS = 7,
    /**
     * ┻の交差点
     */
    INVERT_TCROSS = 8,
    /**
     * ╋形の交差点
     */
    CROSS = 9,
    /**
     * 四角形
     */
    RECTANGLE = 10,
    /**
     * ひし形
     */
    ARGYLE = 11,
    /**
     * ×
     */
    XMARK = 12,
    /**
     * V字
     */
    VFORMATION = 13,
    /**
     * ・
     */
    DOT_MARK = 14,
    /**
     * 全点灯
     */
    ALL_LIGHT = 15
}ShapeType;

/**
 * 表示するパターン
 */
typedef struct{
    unsigned char _column[8];
}Pattern;

/**
 * 初期化する
 */
void DotMatrixDisplay_initialize(void);

/**
 * 表示するパターンを設定する
 * @param type パターン
 */
void DotMatrixDisplay_setPattern(ShapeType type) ;

/**
 * オリジナルパターンを設定する
 * @param original_pattern 表示するオリジナルパターン
 */
void DotMatrixDisplay_setOriginalPattern(Pattern *original_pattern);

/**
 * パターンを表示する
 */
void DotMatrixDisplay_turnOn(void);

/**
 * パターンを非表示にする
 */
void DotMatrixDisplay_turnOff(void);
#endif	/* _DOTMATRIXDISPLAY_H_ */

