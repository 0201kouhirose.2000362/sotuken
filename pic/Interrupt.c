#include <xc.h>
#include <stdio.h>
#include "interrupt.h"

/**
 * タイマ0の割込みが発生したときに呼び出されるリスナ
 */
static void (*_timer0Listener)() = NULL;

/**
 * I2Cに関する割り込みが発生したときに呼び出される関数
 */
static void (*_i2CListener)()= NULL ;

/**
 * INTに関する割り込みが発生したときに呼び出される関数
 */
static void (*_ReceiveListener)()=NULL;

void Interrupt_setTimer0Listener( void(*listener)()){
    _timer0Listener = listener;
}

void Interrupt_setI2CListener( void(*listener)()){
    _i2CListener = listener;
}

void Interrupt_setReceiveListener(void (*listener)()){
    _ReceiveListener = listener;
}

static void __interrupt() _interuptHandler(void) {

    /* 割り込みを行えないようにする */
    PEIE = 0;

    /* I2Cに関する割り込みが発生したら */
    if(SSPIF && _i2CListener!=NULL){
        (*_i2CListener)();
        /* フラグをクリアする */
        SSPIF = 0;
    }

    /* タイマ0の割り込みが発生したら */
    if (T0IF == 1 && _timer0Listener!=NULL) {
        (*_timer0Listener)();
        /* タイマ0の割り込みクリア */
        T0IF = 0;
    }
    
    /* INTの割り込みが発生したら(ENが立ち上がったら)*/
    if(INTF ==1&& _ReceiveListener!=NULL){
       (*_ReceiveListener)();
        /*INTの割り込みをクリア*/
        INTF = 0;
    }

    /* 割り込みを行えるようにする */
    PEIE = 1;
}
